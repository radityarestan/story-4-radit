from django.conf.urls import url
from . import views

app_name = 'story_3'

urlpatterns = [
    url(r'^$', views.home, name = 'home'),
    url(r'^activities/$', views.activities, name = 'activities'),
    url(r'^my_project/$', views.my_project, name = 'my_project'),
    url(r'^about_me/$', views.about_me, name = 'about_me'),
]