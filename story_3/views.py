from django.shortcuts import render

# Create your views here.

def home(request) :
    return render(request, 'home/home.html')

def activities(request) :
    return render(request, 'home/activities.html')

def my_project(request) :
    return render(request, 'home/my_project.html')

def about_me(request) :
    return render(request, 'home/about_me.html')
